A repo containing my python exercises in order to improve my python3 libraries understanding.

1) CVE-2019-11510 : porting the code of CVE-2019-11510 on python3 (https://codeberg.org/AmenoCat/CVE-2019-11510-PoC)
2) CVE-2017-6017 : PoC for modbus injection in Schneider PLCs that lead to DoS
3) WORK IN PROGRESS.
